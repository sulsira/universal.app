<?php namespace Universal\Calculations;

class Rent{

	protected $attributes;
	protected $amount_paid = 0;
	protected $total = 0;
	protected $date_paid;
	protected $number_months;
	public $monthly_fee;
	public $details;
	protected $pre_balance;
	protected $cal_balance;
	protected $total_balance;
	protected $last_paid_date;
	protected $owning_days;
	protected $owning_months;
	protected $nextpaydate;
	public function __construct($attributes = null, $details = array()){

		$this->attributes = (!empty($attributes))? $attributes : \Input::all();
		$this->details = (!empty($details))? $details : [];
		if (empty($this->details)) {
			$house = \House::find($this->attributes['payment_house']);
			$this->details['rent_nextpaydate'] = null;
			$this->details['rent_balance'] = 0;
			$this->details['rent_monthlyFee'] = $house->hous_price;
			// $this->details['rent_monthlyFee'] = null;
			
			// dd($house);
		}
	}


	private function addDays(){
		// we want to get the number months he paid for
			// if the user supply the months we should dive that by the total money he paid
			// the we check to see if the same amount equal to his monthly fee
			// if not then we have to check and see from the montly payment how many months he paid fully 
			// then we check is that equal or greater than what the user enter as number of months
			// if its less than the number of months the - to identify owing due; else then we positive number to show over deposit
		// we want to know how much for each month
		// we want to know when he paid
		// we want to know from the day he paid when should be pay again
	}
	private function subDays(){

	}
	private function subMonths(){
		
	}
	private function addMonths(){
		
	}	

	private function checkbalance(){
		
	}
	public function getPaymentFee(){

	}
	public function getNextpayDay(){
		// next pay day is equal to number_of_months_paid times month_days plus paid date 

		if ($this->number_months) {


			$date = new \DateTime($this->attributes['date_paid']);

			$t = $this->number_months ?: 0;

			$interval = new \DateInterval("P$t".'M');

			$date->add($interval);
			$this->nextpaydate =  $date->format('Y-m-d');


		 } 



	}
	public function getowingmonths(){
		// $this->attributes['date_paid']
		// owing months is equal to next paid date > today
		if ($this->details['rent_nextpaydate']) {

			$the_Date = $this->details['rent_nextpaydate'];
			$today = strtotime("now");

			$HH = strtotime($the_Date);

			if ($HH > $today) {
				$dat = strftime('%F',$HH);
				$now = strftime('%F',$today);
				$datetime1 = new \DateTime($dat);
				$datetime2 = new \DateTime($now);
				$interval = $datetime2->diff($datetime1);
				$this->owning_days =  $interval->format('%a');
				$this->owning_months =  $interval->format('%m');

			}else {
				$this->owning_days =  0;
				$this->owning_months =  0;
			}
		}

	}
	public function getBalance(){


			$bal = $this->details['rent_balance'] + $this->cal_balance;

			$this->total_balance = $bal;



	}
	private function calmonths(){
		// get the monthly fee and divide by amount paid would give you the

		$mlf = $this->details['rent_monthlyFee'];
		
		$ap = $this->attributes['amount_paid'];


		$this->cal_balance = ($mlf == '0.00')? 0 : $ap % $mlf;

		$paid = $ap - $this->cal_balance;

		$this->amount_paid = $paid;

		$this->number_months = ($mlf == '0.00')? 0 : $paid / $mlf;

		
	}
	public function start(){

			$this->calmonths();
			$this->getBalance();
			$this->getowingmonths();
			$this->getNextpayDay();




	}#end of start


	public function process(){
		
		$this->start();
		$data = array('rent'=> [
			'rent_balance' => $this->total_balance, 
			'rent_nextpaydate' => $this->nextpaydate
		],'payment'=>[
			'paym_forMonths' => $this->number_months,
			'monthsto' => $this->nextpaydate,
			'amount_paid'=>$this->amount_paid

		]);
		return $data;
	}
}
