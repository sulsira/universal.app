<?php namespace Universal\Creation;
/**
* 
*/
use Laracasts\Commander\CommandHandler;
use Universal\DailyTransaction\DailyTransactionRepository;
use Laracasts\Commander\Events\DispatchableTrait;
class DailyTransactionCommandHandler implements CommandHandler
{
	use DispatchableTrait;
	protected $repository;
	function __construct(DailyTransactionRepository $repository){
		$this->repository = $repository;
	}
	public function handle($command){
		$transaction  = \DailyTransaction::creation($command);
		$this->repository->save($transaction);
		$this->dispatchEventsFor($transaction);
	}
}
