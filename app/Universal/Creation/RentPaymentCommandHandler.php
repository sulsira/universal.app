<?php namespace Universal\Creation;
/**
* 
*/
use Laracasts\Commander\CommandHandler;
use Universal\RentPay\RentPaymentRepository;
use Laracasts\Commander\Events\DispatchableTrait;
class RentPaymentCommandHandler implements CommandHandler
{
	use DispatchableTrait;
	protected $repository;
	function __construct(RentPaymentRepository $repository){
		$this->repository = $repository;
	}
	public function handle($command){
		$rentpay  = \Rentpayment::creation($command);
		$this->repository->save($rentpay);
		$this->dispatchEventsFor($rentpay);
	}
}
