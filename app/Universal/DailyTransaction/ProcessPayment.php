<?php namespace Universal\DailyTransaction;
/**
* 
*/
// use Eloquent;
use Luwa, Compound,Rent,Rentpayment;
abstract class ProcessPayment extends \Eloquent
{
	public $current_tenant;
	protected $last_paid_date;
	protected $next_paid_date;
	protected $rent_balance = 0;
	protected $rent_monthlyFee;
	protected $house;
	protected $dates; #in the rent pay table the next rent month
	protected $number_months; # the last paid month
	protected $rent;
	public function execute(){
		$this->getHouse($this->data);

	}
	public function getHouse($data){
		$houses  = [];
		$the_house = [];
		// var_dump($this->data);
		$house = \House::with('tenants.rents.payments')->where('hous_number','=',$data['house_plot_num'])->get();
		// if house is one then everything is correct just go and get the his rent and payments
		if(!isset($this->data['paym_houseID'])):
			$house = \House::with('tenants.rents.payments')->where('hous_number','=',$data['house_plot_num'])->get();
		else:
			$house = \House::with('tenants.rents.payments')->where('hous_id','=',$this->data['paym_houseID'])->get(); 

		endif;
	
		$house = (!empty($house))? $house->toArray() : [];
		
		if (!empty($house)) {
			if (count($house) == 1) {
				$this->getTenant(head($house));
				$this->house = $house;
				$this->rent = head($house);
			}else{
				if(count($house) > 1){
					$this->tooManyHouses($house);
				}
			}
			$this->house = head($house);
			
		}
		// count the results if its more than one 

	}
	private function getTenant($house){
		
		if (!empty($house)) {

			foreach ($house['tenants'] as $tt => $tenant) {
				if ($tenant['tent_status'] == 1) {
					$this->current_tenant = $tenant;

				}
			}
		}
		$this->getLastPaid($this->current_tenant['rents']);
	}
	private function getLastPaid($rents){
		if(!empty($rents)){
			$last_rent = last($rents);
			if(isset($last_rent['rent_lastpaydate']) && !empty($last_rent['rent_lastpaydate'])){
				$this->last_paid_date = $last_rent['rent_lastpaydate'];
			}else{
				$this->last_paid_date = false;
			}
			if (isset($last_rent['rent_nextpaydate']) && !empty($last_rent['rent_nextpaydate'])) {
				$this->next_paid_date = $last_rent['rent_nextpaydate'];
			}else{
				$this->next_paid_date = false;
			}
			if (isset($last_rent['rent_balance']) && !empty($last_rent['rent_balance'])) {
				$this->rent_balance = $last_rent['rent_balance'];
			}
			if (isset($last_rent['rent_monthlyFee']) && !empty($last_rent['rent_monthlyFee'])) {
				$this->rent_monthlyFee = $last_rent['rent_monthlyFee'];
			}else{
				$this->rent_monthlyFee = $this->house['hous_price'];
			}

		}else{
			$this->createNewRent();
		}

		$this->processRentPay();
	}
	public function tooManyHouses($houses){

	}
	private function createNewRent(){
		// no rent was ever paid
		// first get the house
		// set the rent_monthly fee;
	}
	private function get_year_month_day(){
		$year = [];
		// conver the date into an array of 
		// year = ['month'=> 1, 'day' => 28];
		if ($this->next_paid_date) {
			// die(var_dump($this->next_paid_date));
			$date = $this->date_breaker($this->next_paid_date);
			if (!empty($date)) {
				$year[$date['year']] = ['day' => $date['day'], 'month'=> $date['month']];
			}

		}
		$this->dates = $year;
	}
	public function date_breaker($input){
		$date = [];
		// gets a date and breaks it into day,month,year
		$dat = new \DateTime($input);
		$date['day'] = $dat->format('d');
		$date['month'] = $dat->format('m');
		$date['year'] = $dat->format('Y');
		
		return $date;
		// str_contains('Hello foo bar.', 'foo');
	}
	private function processRentPay(){
		if ($this->rent_monthlyFee) {

				$this->compare();
				//check into rentpay table and if there not results which mean you have to create a new 
			// check if its not null the last paid 
				// if ($this->last_paid_date) {
				// 	# code...
				// }
				// if true 
				// compare and the payrent table last filled month
					//get the month and year of the last paid date of the payrent table
					// if its true then call the call calculate months being paid for
					// then pass the value to 
		}else{
			// there no way to continue processing
		}
		// it checks for the monthly fee if not then stop processing
		// checks for lastpaid date and 
	}
	private function compare(){
			$year = new \DateTime('y');
			$now = $year->format("Y");
		// check to see if we can identify the house or tenant
		if (!empty($this->current_tenant) && isset($this->current_tenant['tent_id'])) {
			$rp  = \RentPay::whereRaw('tenant_id = ? AND year = ?',[$this->current_tenant['tent_id'], $now ])->first();
			if ($rp) {
				$this->getLastFilled($rp);
			}else{
				$this->generateNewRent();
				$this->insertRent();
			}
			

		}
		
	}
	private function generateNewRent(){
			// if rent pay does not exist we have to create a new one

			// how

				// you check and see if the tenant has last pay day or next pay day

					if ($this->last_paid_date !== false || $this->next_paid_date !== false) {
						// if true then check for next pay day first 
							if ($this->next_paid_date) {
								$this->processNextPayDate($this->next_paid_date);
							}else{

								if ($this->last_paid_date) { // if true then trust the value
									$this->processNextPayDate($this->last_paid_date);
								}

							}

					}else{

						$this->processNextPayDate();
					}
						
						// take the date and and fill the rent table until the month he next pay day 
						// create a new next pay date and save it in scope / replace the next pay date -
							// check to see if the new pay date is within this year or proceed to another years
							// if proceeded then subtract from this year every year to next pay date year
						// then take current payment being made and calcute the amount of months being paid for 
							// if there is balace and it to balance
							// check to see if the balance can pay for another month if yes convert it into months paid and add it to the months paid
						// then get the last null month value in the payrent table
						// add the months by update to the rent pay table
						// then you are done.
						// return 
					// else if not true ( and next pay date is false )
						// check to see if last paid date is available
							//if true create a new next pay date pay date
						// add the values to both the pay rent table and update teh rent table
					// then you are done
	}
	private function payRent(){
		// fine month for which insert/update in the pay rent table
		// 
	}
	private function updateRent($rent){
		// die(var_dump($rent));
	}
	private function insertRent(){
		// put it luwas
		$rent =last($this->current_tenant['rents']);
		// var_dump($this->current_tenant);
		$house = \House::with('compound.landlord')->where('hous_id','=',$this->current_tenant['tent_houseID'])->first();
		// $house = \House::find($this->current_tenant['tent_houseID'])->compound();

		$house = (!empty($house))? $house->toArray() : $house;
		// var_dump(last($this->current_tenant['rents']));
		// var_dump($house);
		// var_dump($rent);
		// var_dump($this->data);
		$range = $this->monthl();
		$amount_paid = (!isset($this->userInput['amount']))? $this->userInput['amount_paid']  : $this->userInput['amount'];
		$i = 0;
		//get all from rent-table
					$new_date = new \DateTime('now');
					$now = $new_date->format("Y");
		$rp  = \RentPay::whereRaw('tenant_id = ? AND year = ?',[$this->current_tenant['tent_id'], $now  ])->first();
		if (!$rp) {
			foreach ($range  as $year => $month):
					$luwa = new \RentPay;
					$luwa->tenant_id = $this->current_tenant['tent_id'];
					$luwa->compound_id = $house['compound']['comp_id'];
					$luwa->ll_id = $house['compound']['landlord']['id']; 
				foreach($month as $ind=>$value):
					$luwa->$value = $this->rent_monthlyFee;  

				endforeach;
			$luwa->year = $year;
			$luwa->save();
			// 	$i++;				
			endforeach;
		}else{
			foreach ($range  as $year => $month):
					$luwa = RentPay::find($rp->id);
				foreach($month as $ind=>$value):
					$luwa->$value = $this->rent_monthlyFee;  
				endforeach;
			$luwa->save();
			endforeach;
		}
		// we loop and compare the months if the months are not the same and their values are not null
		// then we update and move on
		foreach ($range  as $year => $month):
			foreach($month as $ind=>$value):
				$luwa = new Luwa;
				$luwa->tenant_id = $this->current_tenant['tent_id'];
				$luwa->compound_id = $house['compound']['comp_id'];
				$luwa->ll_id = $house['compound']['landlord']['id']; 
				$luwa->house_id = $this->current_tenant['tent_houseID'];
				$luwa->month_num = $ind+1 ;
				$luwa->month_short = $value;
				$luwa->amount = $this->rent_monthlyFee;			
				$luwa->year = $year;
				$luwa->amount_paid = $amount_paid ;
				$luwa->save();
			endforeach;
			$i++;				
		endforeach;

		$renting = Rent::find($rent['rent_id']);
		$renting->rent_lastpaydate = $this->last_paid_date;
		$renting->rent_nextpaydate = $this->next_paid_date;
		$renting->rent_balance = $this->rent_balance;
		$renting->save();

		if(!isset($this->data['paym_houseID'])):
			Rentpayment::create([
			'paym_rentID' => $rent['rent_id'],
			'paym_houseID' => $this->current_tenant['tent_houseID'],
			'paym_forMonths' => $this->number_months ,
			'paym_paidAmount' => $this->userInput['amount'],
			'paym_date' =>  $this->userInput['date_paid'],
			'paym_balance' => $this->rent_balance,
			'monthsto' => $this->next_paid_date,
			'paym_remarks'=> $this->userInput['description']
			]);
		endif;
		// and update rent or add rent
	}
	private function monthl(){
		$container = [];
		$date_paid = $this->date_breaker($this->data['date_paid']);
		$las_dat = $this->date_breaker($this->next_paid_date);
		foreach ($this->dates as $year => $monthday) {
			for($y = $date_paid['year']; $y <= $year; ++$y ){				
					foreach ($this->mons as $key => $value) {

							if($y == $date_paid['year'] || $y == $year){
								if ($key >= $date_paid['month']) {
									$container[$y][] = $value;
								}else

								if($y == $year){
									if($key >= $las_dat['month']){
										$container[$y][] = $value;
									}
									
								}

							}else{

								$container[$y][] = $value;								
							}
						}	

			}
		}
		return $container;

	}
	private function getLastFilled($rp){

		$this->generateNewRent();
		$this->updateRent($rp);
	}
	private function processNextPayDate($date = null){

		// get the current payment
			// var_dump($this->data);
			// convert payment in months and balance
			$months = $this->paymonths();
			// $this->number_months
			$this->getNextPayDate($months);

		// convert it in to month and balace
			$this->get_year_month_day();
	}
	private function paymonths(){
		// months = (amount /   monthly_fee  )


		// balance = (rent_balance + (amount % monthly_fee)) --


		// new_balance = ((amount_paid + balance) % monthly_fee) --


		// amount = (amount_paid + balance) - new_balance --


		// monthly_fee = rent_monthly_fee || house_monthly_fee --

		// new_place should replace balance
		// var_dump();
		// var_dump($this->data);
		// var_dump($this->userInput);
		// die();
		$amount_paid = (isset($this->data['paym_paidAmount']))? $this->data['paym_paidAmount'] : $this->data['amount'] ;
		$monthly_fee  = $this->rent_monthlyFee;
		$balance = ($this->rent_balance + ($amount_paid  % $monthly_fee));
		$new_balance = (($amount_paid  + $balance ) % $monthly_fee);
		$amount = ($amount_paid  + $balance) - $new_balance;
		$months = ($amount /   $monthly_fee  );
		$this->rent_balance = $new_balance; 
		$this->number_months .= $months;
		return $months;
	}
	private function getNextPayDate($months){
		$this->next_paid_date = $this->getAddDates($months, $this->next_paid_date);	
		
	}
	private function getAddDates($moths, $date = false){
				// create a new next pay date
			// add the payment date to the next pay date
			$the_date;
			if ($date != false) {

				// add the old date with this new date
					$new_date = new \DateTime($date);

					$t = $moths ?: 0;

					$interval = new \DateInterval("P$t".'M');

					$new_date->add($interval);
					$the_date =  $new_date->format('Y-m-d');


			}else{

				// this new date replace the false value
				$new_date = new \DateTime($this->data['date_paid']);

					$t = $moths ?: 0;

					$interval = new \DateInterval("P$t".'M');

					$new_date->add($interval);
					$the_date =  $new_date->format('Y-m-d');

			}
			return $the_date;

	}

}

	// daily house from the house #
	// get the current tenant of the house
	// get his rent payment, his : payments, house, price 
	// get his last payment date
		// if not
		// create his last payment date
		// look in to his rent pays year ( get the current year we are and get the same year row from the table)
	// loop through that results and see the find the first null month
	// replace first null month with the global or new payment date
		// but first convert the date month into a number
		// right now we should know
			// 1. an array of the number of the month and the value text
			// 2. we should get the amount of months he paid for
			// 3. we should get the last month he paid for and the and the amount
			// 4. we should the first null month in the table
			// 5 we should get his monthly fee payment either from rent or house
	// process how many months to be field with payments
	// file in the database with this months by update
	