<?php namespace Universal\Forms;

use Laracasts\Validation\FormValidator;

class CreateTenant extends FormValidator{

	protected $rules = [
		'person.fname'=> 'required|max:200',
		'person.mname'=> 'max:200',
		'person.lname'=> 'required|max:200',
		'person.dob'=> 'date',
		'person.Pers_Ethnicity'=> 'max:200',
		'person.Pers_Nationality'=> 'max:200',
		'person.Pers_Gender'=> 'max:200',
		'photo'=>'mimes:jpg,png,jpeg|max:3024', #this should more in the future
		'document'=>'mimes:pdf:6024', #this should more in the future
		'address.Addr_Town'=> 'max:200',
		'address.Addr_District'=> 'max:200',
		'address.Addr_AddressStreet'=> 'max:200',
		'contact.phones'=> 'max:200',
		'contact.email'=> 'max:220',
		'contact.telephone'=> 'max:200'
	];
		/**
		 * validation rules for the plot creation form
		 * Post /plots/create
		 *
		 * @return Response 
		 */



}