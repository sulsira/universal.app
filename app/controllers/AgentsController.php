<?php

class AgentsController extends \AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /agents
	 *
	 * @return Response
	 */
	public function index()
	{
		$agents = Agent::with('person.contacts')->get();	
		$agents = $agents ? $agents->toArray() : []; #secure
		$this->layout->content = View::make('admin.agents.index')->with('agents',$agents);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /agents/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('admin.agents.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /agents
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$person = array();
		$staff = array();
		$address = array();
		$contact = array();
		$done = false;

		if ($input) :
			foreach ($input as $k => $table) {
				if (is_array($table)) {
					if ($k == 'person') {
						$V = new services\validators\Person($table);
						if($V->passes()){
							$person = Person::create(array(
							'pers_fname' => $table['fname'], 
							'pers_mname' => ($table['mname']) ?: null, 
							'pers_lname' => $table['lname'], 
							'pers_type' => 'Agent', 
							'pers_DOB' => $table['dob'], 
							'pers_gender' => $table['Pers_Gender'], 
							'pers_nationality' => $table['Pers_Nationality'], 
							'pers_ethnicity' => $table['Pers_Ethnicity'], 
							));
							if ($person->id) {
								$done = true;
							}
						}else{
							$errors = $V->errors;
							return Redirect::back()->withErrors($errors)->withInput();							
						}


					}
					if ($k == 'address') {
						$address = $table;
						if ($person->id) {
							$address = array_add($address, 'Addr_EntityID', $person->id);
							$address = array_add($address, 'Addr_EntityType', 'Person');
							$V = new services\validators\Address($table);
							if($V->passes()){
								$address = Address::create($address);
							}
						}
						$errors = $V->errors;

					}
					if ($k == 'contact') {

						

						if ($person->id) {

							$V = new services\validators\Contact($table);
							foreach ($table as $key => $value) {
								if($V->passes()){

									if(!empty($value)){
										$contact = Contact::create(array(
										'Cont_EntityID' => $person->id,	
										'Cont_EntityType' => 'Person',	
										'Cont_Contact' => $value,	
										'Cont_ContactType' =>  $key	
										));

									$contact = $contact->toArray();
									}
								}
							}
							$errors = $V->errors;
						}
					}

				}
			}
			if($done){
					Agent::create(array(
						'agen_persID'=> $person->id,
						'agen_contID'=> $contact['id'],
						'agen_addrID'=> ($address->id) ?: 0
					));
				Flash::message("Successfully added a Student");
				return Redirect::back();
			}else{
				return Redirect::back()->withErrors($errors)->withInput();							
			}
		endif;


	}

	/**
	 * Display the specified resource.
	 * GET /agents/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

		// $agents = Plot::with('estate')->get();
		// $agents = Estate::with('plots')->get();
		$agents = Agent::with('person.contacts','person.addresses','plots.estate','plots.customer.person','person.documents')->where('agen_id','=',$id)->first();

		$agent = $agents ? $agents->toArray() : []; #secure
	

		$this->layout->content = View::make('admin.agents.show')->with('agent', $agent);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /agents/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$agents = Agent::with('person.contacts','person.addresses','plots.estate','plots.customer.person','person.documents')->where('agen_id','=',$id)->first();

		$agent = $agents ? $agents->toArray() : []; #secure

		$this->layout->content = View::make('admin.agents.edit')->with('agent', $agent);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /agents/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{	
		$input = Input::all();
		if( isset($input['cross']) && $input['cross'] == 28):
			$plot = Plot::find($input['plot_ID']);
			if($plot):
				$plot->plot_agenID = $id;
				$plot->save();
			endif;
							// Flash::message("Successfully added a Customer");
				return Redirect::back();
		endif;

		if($input['type'] == 'person'):
			$person = Person::findOrFail($input['person_id']);
			$person->fill($input);
			$person->save();
			return Redirect::back();
		endif;

		if($input['type'] == 'address'):

			$person = Address::findOrFail( $input['address_id'] );
			$person->fill($input);
			$person->save();
			return Redirect::back();	
		endif;

		if($input['type'] == 'contact'):

			$person = Contact::findOrFail( $input['contact_id'] );
			$person->fill($input);
			$person->save();
			return Redirect::back();

		endif;

		if($input['type'] == 'image' && !empty(Input::file('photo'))):

			$path = (empty($input['foldername']))? base_path().DS.'media/Agent'.DS.$input['folder'] : base_path().DS.$input['foldername'] ; #notice 006 would fail if not set
			$not =  '';
			$foldername = '';
			$fn = '';
			if(starts_with($input['foldername'], '/')):

				$person = Document::find($input['image_id'])->person()->first();
				$not = 'media'.DS.'Agent'.DS.$person->pers_fname.'_'.$person->pers_mname.'_'.$person->pers_lname.'_'.$person->id;
				$foldername = DS.$not.DS;
				$fn .= $not;

			else:

				$not = (empty($input['foldername']))? 'media/Agent'.DS.$input['folder'] : $input['foldername'];
				$foldername = DS.$not.DS;

			endif;	
			$thumbnail_dir = $not.DS.'img'.DS.'thumbnail'.DS;

			$fileexten = Input::file('photo')->getClientOriginalExtension();

			$filename = Input::file('photo')->getClientOriginalName();

			$shortened = base_convert(rand(10000,99999), 10, 36);



					if (File::exists($path)) {

					// 	// the folder exist then add a mew  picture and record in databse
					// 	// disable or delete the previous img
							

							


							$rename = 'renamed_'.$shortened.'_photo_'.$filename;

							File::exists($path.DS.'img'.DS.'thumbnail') ?: mkdir($path.DS.'img'.DS.'thumbnail', 0755, true); #notice 007 a hack 

								 Input::file('photo')->move($not.DS.'img'.DS , $rename);

									$img = Image::make($not.DS.'img'.DS.$rename);

									$img->resize(250 , 300);

									$img->save($thumbnail_dir.$rename);	// we move to thumnails

								// store in the database

									if(isset($input['image_id'])): 
										
										$document = Document::findOrFail( $input['image_id'] );
										$document->thumnaildir = $thumbnail_dir.$rename;
										$document->fullpath = $input['foldername'].'img'.DS.$rename;
										$document->filename = $filename ;
										if($fn):
											$document->foldername = $not;
										endif;
										$document->extension = $fileexten;
										$document->save();

									else:

										$document = new Document;
										$document->thumnaildir = $thumbnail_dir.$rename;
										$document->entity_type = 'Person';
										$document->type = 'Photo';
										$document->entity_ID = $input['person_id'];
										$document->fullpath = $input['foldername'].'img'.DS.$rename;
										$document->filename = $filename ;
										$document->foldername = $not;
										$document->extension = $fileexten;
										$document->save();

									endif;


								return Redirect::back();	
															
					}else{

							// $full_dir = $foldername.'img'.DS.'full';

							$rename = 'newly_'.$shortened.'_photo_.'.$filename;


							$filetyp = Input::file('photo')->getMimeType();

							$fileexten = Input::file('photo')->getClientOriginalExtension();
							$uploaded = false;


							 File::exists($path) ?: mkdir($path, 0755, true);

							 File::exists($path.DS.'img'.DS) ?: mkdir($path.DS.'img'.DS, 0755, true);

								// var_dump($not.DS.'img'.DS , $rename);
								File::exists($path.DS.'img'.DS.'thumbnail') ?: mkdir($path.DS.'img'.DS.'thumbnail', 0755, true);

								
								// Input::file('photo')->move($not.DS.'img'.DS , $rename);
									// we resize (250 X 300)
								 

								 Input::file('photo')->move($not.DS.'img'.DS , $rename);

									$img = Image::make($not.DS.'img'.DS.$rename);

									$img->resize(250 , 300);

									$img->save($thumbnail_dir.$rename);	// we move to thumnails


									if(isset($input['image_id'])): 

										$document = Document::findOrFail( $input['image_id'] );
										$document->thumnaildir = $thumbnail_dir.$rename;
										$document->fullpath = $input['foldername'].'img'.DS.$rename;
										$document->filename = $filename ;
										$document->foldername = $not;
										$document->extension = $fileexten;
										$document->save();

									else:

										$document = new Document;
										$document->thumnaildir = $thumbnail_dir.$rename;
										$document->fullpath = $input['foldername'].'img'.DS.$rename;
										$document->entity_type = 'Person';
										$document->type = 'Photo';
										$document->entity_ID = $input['person_id'];
										$document->filename = $filename ;
										$document->foldername = $not;
										$document->extension = $fileexten;
										$document->save();

									endif;


					}



		endif;	
		return Redirect::back();	

	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /agents/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Agent::destroy($id);
		return Redirect::back();
	}

}