<?php

class EstatesController extends AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /estates
	 *
	 * @return Response
	 */
	public function index()
	{
		// $estate  = Estate::find(5)->plots;
		$est = Request::segment(2);
		if($est):

		// $estate  = Estate::with('plots.customer.person')->first();
		$estate  = Estate::with('plots.customer.person')->where('est_id','=',$est)->first();


		$estate = $estate ? $estate->toArray() : [];

		$this->layout->content = View::make('admin.Estates/plots.index')->with('estate',$estate);	
		else:
		$estate  = Estate::with('plots')->get();
		$estate = $estate ? $estate->toArray() : [];
		$this->layout->content = View::make('admin.Estates.index')->with('estate',$estate);			
		endif;

	}

	/**
	 * Show the form for creating a new resource.
	 * GET /estates/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$est = Request::segment(2);
		if( $est ):
			$estate  = Estate::where('est_id','=',$est)->first();
			$estate = $estate ? $estate->toArray() : [];
			$this->layout->content = View::make('admin.Estates/plots.create')->with('estate',$estate);	
		else:


		endif;
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /estates
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();

		$est = Estate::create(array(
			'name'=> $input['est_name'],
			'location'=> $input['est_location']
		));
		if ($est) {
				Flash::success('Your have added an estate');
				return Redirect::back();
		}
	}

	/**
	 * Display the specified resource.
	 * GET /estates/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /estates/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
			$estate  = Estate::find($id);
			// $estate = $estate ? $estate->toArray() : [];
			$this->layout->content = View::make('admin.Estates.edit')->with('estate',$estate);	
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /estates/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$person = Estate::findOrFail( $id );
		$person->fill(Input::all());
		$person->save();
		return Redirect::to('estates');	
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /estates/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Estate::destroy($id);
		return Redirect::back();
	}

}