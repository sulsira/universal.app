<?php

class LuwasController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /luwas
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /luwas/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /luwas
	 *
	 * @return Response
	 */
	public function store($data)
	{	
		$input = Input::all();
		if(isset($input['paid_for'])):
			if ($input['paid_for'] == 'rent'):
				
			endif;	
		endif;
		// die(var_dump($data));
	}

	/**
	 * Display the specified resource.
	 * GET /luwas/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /luwas/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /luwas/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /luwas/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	private function getTenant($house){
		if (!empty($house)) {

			foreach ($house['tenants'] as $tt => $tenant) {
				if ($tenant['tent_status'] == 1) {
					$this->current_tenant = $tenant;
				}
			}
		}
		$this->getLastPaid($this->current_tenant['rents']);
	}

}