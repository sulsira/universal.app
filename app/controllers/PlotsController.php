<?php

class PlotsController extends AdminController {



	/**
	 * Display a listing of the resource.
	 * GET /plots
	 *
	 * @return Response
	 */
	public function index()
	{

		$plots = Plot::with('customer','customer.person')->get();
		
		$plots = $plots ? $plots->toArray() : []; #secure

		$this->layout->content = View::make('admin.plots.index')->with('plots',$plots);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /plots/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('admin.plots.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /plots
	 *
	 * @return Response
	 */
	public function store()
	{
		$plot = array();
		$input = Input::all();
		$p  = new services\Validators\Plot;
		if($p->passes()){

			if (isset($input['est_id'])) {
				
				$plot = Plot::create(array(
					'plot_location' => $input['plot_location'],
					'plot_price' => $input['plot_price'],
					'plot_size' => $input['plot_size'],
					'plot_number' => $input['plot_number'],
					'plot_remarks' => $input['plot_remarks'],
					'plot_estateID' => $input['est_id'],
					'plot_availability' => 'available'
					)
				);
			}else{

				$estate = Estate::firstOrCreate(array('name' => Input::get('est_name'),'location'=>Input::get('plot_location') ));
				$plot = Plot::create(array(
					'plot_location' => $input['plot_location'],
					'plot_price' => $input['plot_price'],
					'plot_size' => $input['plot_size'],
					'plot_number' => $input['plot_number'],
					'plot_remarks' => $input['plot_remarks'],
					'plot_estateID' => $estate->est_id,
					'plot_availability' => 'available'
					)
				);	
							
			}

			if ($plot) {

				Flash::success('Your have added a plot');
				return Redirect::back();
			}
		}else{
			$errors = $p->errors;
			return Redirect::back()->withErrors($errors)->withInput();							
		}
	}

	/**
	 * Display the specified resource.
	 * GET /plots/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$plots = Plot::where('plot_id','=',$id)->first();
		$plot = $plots ? $plots->toArray() : [];
		$this->layout->content = View::make('admin.plots.show')->with('plot',$plots);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /plots/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$plot = Plot::find($id);
		$this->layout->content = View::make('admin.plots.edit',compact('plot'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /plots/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
		if (isset($input['add_plot'])):
			$plot = Plot::findOrFail($input['plot_ID']);
			if(!empty($plot)):
				$plot->plot_status = 1;
				$plot->plot_availability = 'not-available';
				$plot->plot_cusID = $id;
				$plot->save();
			endif;
		else:	

			$plot = Plot::findOrFail($id);
			$plot->fill(Input::all());
			$plot->save();	
				
		endif;

		return Redirect::to('plots');			
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /plots/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Plot::destroy($id);
		return Redirect::back();
	}

}