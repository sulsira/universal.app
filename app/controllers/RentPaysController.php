<?php
use Universal\DailyTransaction\PayRent;
class RentPaysController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /rentpays
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /rentpays/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /rentpays
	 *
	 * @return Response
	 */
	public function store($data = array())
	{

		if(!empty($data)):
			$dailypaid = (isset($data->dailypaid))? $data->dailypaid->toArray() :  [];
			if(!empty($dailypaid)):
				if(isset($dailypaid['paid_for']) && $dailypaid['paid_for'] == 'rent'):

					if($dailypaid['type'] == 'income'):
					$payrent =  new PayRent($dailypaid);
					$payrent->execute();
					// get a class that would pay rent and return true or false
					endif;
				endif;
			endif;
		endif;
		
		if(isset($data->rentpaid)):
			$payrent =  new PayRent($data->rentpaid);
			$payrent->execute();
		endif;
	}

	/**
	 * Display the specified resource.
	 * GET /rentpays/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /rentpays/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /rentpays/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /rentpays/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}