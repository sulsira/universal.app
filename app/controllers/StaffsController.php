<?php

class StaffsController extends AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /staffs
	 *
	 * @return Response
	 */
	public function index()
	{

		$staffs = Staff::with('person.contacts')->get();

		// $staff = (!empty($staffs))? $staffs->toArray() : []; #secure

		// dd($staff);   


		// $staffs = Person::with('contacts')->where('pers_type','=','Staff')->get();
		$staffs = $staffs ? $staffs->toArray() : []; #secure
		$this->layout->content = View::make('admin.staffs.index')->with('staffs',$staffs);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /staffs/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('admin.staffs.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /staffs
	 *
	 * @return Response
	 */
	public function store()
	{
	$input = Input::all();
		$person = array();
		$staff = array();
		$address = array();
		$contact = array();
		$done = false;

		if ($input) :
			foreach ($input as $k => $table) {
				if (is_array($table)) {
					if ($k == 'person') {
						$V = new services\validators\Person($table);
						if($V->passes()){
							$person = Person::create(array(
							'pers_fname' => $table['fname'], 
							'pers_mname' => ($table['mname']) ?: null, 
							'pers_lname' => $table['lname'], 
							'pers_type' => 'Staff', 
							'pers_DOB' => $table['dob'], 
							'pers_gender' => $table['Pers_Gender'], 
							'pers_nationality' => $table['Pers_Nationality'], 
							'pers_ethnicity' => $table['Pers_Ethnicity'], 
							'pers_NIN' => $table['nin_id']
							));
							if ($person->id) {
								$done = true;
							}
						}else{
							$errors = $V->errors;
							return Redirect::back()->withErrors($errors)->withInput();							
						}


					}
					if ($k == 'address') {
						$address = $table;
						if ($person->id) {
							$address = array_add($address, 'Addr_EntityID', $person->id);
							$address = array_add($address, 'Addr_EntityType', 'Person');
							$V = new services\validators\Address($table);
							if($V->passes()){
								$address = Address::create($address);
							}
						}
						$errors = $V->errors;

					}
					if ($k == 'contact') {

						

						if ($person->id) {

							$V = new services\validators\Contact($table);
							foreach ($table as $key => $value) {
								if($V->passes()){

									if(!empty($value)){
										$contact = Contact::create(array(
										'Cont_EntityID' => $person->id,	
										'Cont_EntityType' => 'Person',	
										'Cont_Contact' => $value,	
										'Cont_ContactType' =>  $key	
										));

									$contact = $contact->toArray();
									}
								}
							}
							$errors = $V->errors;
						}
					}
					if ($k == 'staff') { #a risk of error popping
						$staff = new Staff;
						$staff->position  = $table['position'];
						$staff->department  = $table['department'];
						$staff->staff_personid = $person->id ;
						$staff->save();
					}

				}
			}
			if($done){




					if ($input['photo']) {

							$not = 'media'.DS.'staff'.DS.$person->pers_fname.'_'.$person->pers_mname.'_'.$person->pers_lname.'_'.$person->id;
							$foldername = DS.$not.DS;
							// $full_dir = $foldername.'img'.DS.'full';

							$path = base_path().$foldername ;
							$thumbnail_dir = $not.DS.'img'.DS.'thumbnail'.DS;
							$fileexten = Input::file('photo')->getClientOriginalExtension();
							$filename = Input::file('photo')->getClientOriginalName();
							// dd($path);
							$shortened = base_convert(rand(10000,99999), 10, 36);
							$rename = $person->pers_fname.'_'.$shortened.'_photo_.'.$filename;


							$filetyp = Input::file('photo')->getMimeType();

							$fileexten = Input::file('photo')->getClientOriginalExtension();
							$uploaded = false;

							if ((File::exists($path)) && ( File::exists( $path.DS.'img'.DS ) ) && ( File::exists( $path.DS.'img'.DS.'thumbnail') ) ) {

								Input::file('photo')->move($path.DS.'img'.DS, $rename);
									#notice #01
										// there was a problem when trying to resize the image. the fix is go in the project dir (html but ! include)

									// we resize (250 X 300)
									$img = Image::make($not.$rename);
									$img->resize(250 , 300);
									$img->save($thumbnail_dir.$rename);	// we move to thumnails

								// store in the database
								Document::create(array(
										'title' => 'profile picture' ,
										'entity_type' => 'Person',
										'entity_ID' => $person->id,
										'type' => 'Photo',
										'fullpath' => $path.DS.'img'.DS.$rename,
										'filename' => $filename,
										'foldername' => $foldername,
										'extension' => $fileexten,
										'filetype' => $filetyp,
										'thumnaildir' => $thumbnail_dir.$rename
									));
								
								# code...
							}else{
								File::exists($path) ?: mkdir($path, 0755, true);
								File::exists($path.DS.'img'.DS) ?: mkdir($path.DS.'img'.DS, 0755, true);
								File::exists($path.DS.'img'.DS.'thumbnail') ?: mkdir($path.DS.'img'.DS.'thumbnail');
								Input::file('photo')->move($path.DS.'img'.DS , $rename);
									// we resize (250 X 300)
									// dd($not.DS.$rename);
									$img = Image::make($not.DS.'img'.DS.$rename);
									$img->resize(250 , 300);
									$img->save($thumbnail_dir.$rename);	// we move to thumnails

								// store in the database
								Document::create(array(
										'title' => 'profile picture' ,
										'entity_type' => 'Person',
										'entity_ID' => $person->id,
										'type' => 'Photo',
										'fullpath' =>  $path.DS.'img'.DS.$rename,
										'filename' => $filename,
										'foldername' => $foldername,
										'extension' => $fileexten,
										'filetype' => $filetyp,
										'thumnaildir' => $thumbnail_dir.$rename
									));
							}
					} #end of photo




				Flash::message("Successfully added a Staff");
				return Redirect::back();
			}else{
				return Redirect::back()->withErrors($errors)->withInput();							
			}
		endif;
	}

	/**
	 * Display the specified resource.
	 * GET /staffs/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$staffs = Staff::with('person.documents','person.contacts','person.addresses')->whereRaw('id = ?',[$id])->first();
		$staff = (!empty($staffs))? $staffs->toArray() : []; #secure
		$this->layout->content = View::make('admin.staffs.show')->with('staff',$staff);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /staffs/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$staffs = Staff::with('person.documents','person.contacts','person.addresses')->whereRaw('id = ?',[$id])->first();
		$staff = (!empty($staffs))? $staffs->toArray() : []; #secure
		$this->layout->content = View::make('admin.staffs.edit')->with('staff',$staff);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /staffs/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
		if($input['type'] == 'person'):
			$person = Person::findOrFail($input['person_id']);
			$person->fill($input);
			$person->save();
			return Redirect::back();
		endif;

		if($input['type'] == 'address'):

			$person = Address::findOrFail( $input['address_id'] );
			$person->fill($input);
			$person->save();
			return Redirect::back();	
		endif;

		if($input['type'] == 'contact'):

			$person = Contact::findOrFail( $input['contact_id'] );
			$person->fill($input);
			$person->save();
			return Redirect::back();	
		endif;

		if($input['type'] == 'image' && !empty(Input::file('photo'))):


			$path = (empty($input['foldername']))? base_path().DS.'media/staff'.DS.$input['folder'] : base_path().DS.$input['foldername'] ; #notice 006 would fail if not set
			$not =  '';
			$foldername = '';
			$fn = '';

			if(starts_with($input['foldername'], '/')):

				$person = Document::find($input['image_id'])->person()->first();
				$not = 'media'.DS.'staff'.DS.$person->pers_fname.'_'.$person->pers_mname.'_'.$person->pers_lname.'_'.$person->id;
				$foldername = DS.$not.DS;
				$fn .= $not;
				
			else:

				$not = (empty($input['foldername']))? 'media/staff'.DS.$input['folder'] : $input['foldername'];
				$foldername = DS.$not.DS;

			endif;

			$thumbnail_dir = $not.DS.'img'.DS.'thumbnail'.DS;
			if(!File::exists($thumbnail_dir)):
				mkdir($thumbnail_dir, 0755, true);
			endif;
			$fileexten = Input::file('photo')->getClientOriginalExtension();

			$filename = Input::file('photo')->getClientOriginalName();

			$shortened = base_convert(rand(10000,99999), 10, 36);



					if (File::exists($path)) {

					// 	// the folder exist then add a mew  picture and record in databse
					// 	// disable or delete the previous img

							$rename = 'renamed_'.$shortened.'_photo_'.$filename;

							File::exists($path.DS.'img'.DS.'thumbnail') ?: mkdir($path.DS.'img'.DS.'thumbnail', 0755, true); #notice 007 a hack 

								 Input::file('photo')->move($not.DS.'img'.DS , $rename);

									$img = Image::make($not.DS.'img'.DS.$rename);

									$img->resize(250 , 300);

									$img->save($thumbnail_dir.$rename);	// we move to thumnails

								// store in the database
									// var_dump($thumbnail_dir.$rename);
									// dd($fn);
									// die();
									if(isset($input['image_id'])): 
										
										$document = Document::findOrFail( $input['image_id'] );
										$document->thumnaildir = $thumbnail_dir.$rename;
										$document->fullpath = $input['foldername'].'img'.DS.$rename;
										$document->filename = $filename ;
										if($fn):
											$document->foldername = $not;
										endif;
										$document->extension = $fileexten;
										$document->save();

									else:

										$document = new Document;
										$document->thumnaildir = $thumbnail_dir.$rename;
										$document->entity_type = 'Person';
										$document->type = 'Photo';
										$document->entity_ID = $input['person_id'];
										$document->fullpath = $input['foldername'].'img'.DS.$rename;
										$document->filename = $filename ;
										$document->foldername = $not;
										$document->extension = $fileexten;
										$document->save();

									endif;


								return Redirect::back();	
															
					}else{

							// $full_dir = $foldername.'img'.DS.'full';

							$rename = 'newly_'.$shortened.'_photo_.'.$filename;


							$filetyp = Input::file('photo')->getMimeType();

							$fileexten = Input::file('photo')->getClientOriginalExtension();
							$uploaded = false;


							 File::exists($path) ?: mkdir($path, 0755, true);

							 File::exists($path.DS.'img'.DS) ?: mkdir($path.DS.'img'.DS, 0755, true);

								// var_dump($not.DS.'img'.DS , $rename);
								File::exists($path.DS.'img'.DS.'thumbnail') ?: mkdir($path.DS.'img'.DS.'thumbnail', 0755, true);

								
								// Input::file('photo')->move($not.DS.'img'.DS , $rename);
									// we resize (250 X 300)
								 

								 Input::file('photo')->move($not.DS.'img'.DS , $rename);

									$img = Image::make($not.DS.'img'.DS.$rename);

									$img->resize(250 , 300);

									$img->save($thumbnail_dir.$rename);	// we move to thumnails


									if(isset($input['image_id'])): 

										$document = Document::findOrFail( $input['image_id'] );
										$document->thumnaildir = $thumbnail_dir.$rename;
										$document->fullpath = $input['foldername'].'img'.DS.$rename;
										$document->filename = $filename ;
										$document->foldername = $not;
										$document->extension = $fileexten;
										$document->save();

									else:

										$document = new Document;
										$document->thumnaildir = $thumbnail_dir.$rename;
										$document->fullpath = $input['foldername'].'img'.DS.$rename;
										$document->entity_type = 'Person';
										$document->type = 'Photo';
										$document->entity_ID = $input['person_id'];
										$document->filename = $filename ;
										$document->foldername = $not;
										$document->extension = $fileexten;
										$document->save();

									endif;


					}




		endif;	
		return Redirect::back();	
				// 		Flash::message("Successfully updated a Staff");
				// return Redirect::back();
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /staffs/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$staff = Staff::with('person')->where('id','=',$id)->first();
		$staff = !empty($staff)? $staff->toArray() : $staff;
		// delete person first
			if(isset($staff['person']) && !empty($staff['person'])):
				Person::destroy($staff['person']['id']);
			endif;
		Staff::destroy($id);
		return Redirect::to('/staffs');
	}

}