<?php
use Universal\Calculations\Rent as R;
use Universal\Forms\RentPay;
use Universal\Creation\RentPaymentCommand;
use Universal\Core\CommandBus;
class TenanPaymentsController extends BaseController {


	use CommandBus;
	protected $CommandBus;
	protected $rentpay;
	protected $layout = 'admin';

	function __construct(RentPay $rentpay){
		$this->rentpay = $rentpay;
	}

	/**
	 * Display a listing of the resource.
	 * GET /tenanpayments
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /tenanpayments/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /tenanpayments
	 *
	 * @return Response
	 */
	public function store()
	{

		// validate in global scope
			$this->rentpay->validate(Input::all());
		// create using commander
			$command = new RentPaymentCommand(); //DTO
			$this->execute($command);
			
		// execute everything created


		// send a message and tell where sucessfull

		// go back
		// $input  = Input::all();

		// die(var_dump($input));

		// die();
		// $rentid = Session::get('rentid');
		// $userid = Session::get('user_id');

		// $rent = Rent::where('rent_tenantID','=',$input['tent_id'])->first();

		// $rent = (!empty($rent))?$rent->toArray(): [];

		// $paying = new R($input, $rent );
		// $get = $paying->process();
		// $nrents = 0;
		// if(empty($rent)):
		// 	$nrent = new Rent;
		// 	$nrent->rent_houseID = (!isset($input['payment_house']))? 0 : $input['payment_house'];
		// 	$nrent->rent_tenantID = $input['tent_id'];
		// 	$nrent->save();
		// 	$nrents = $nrent->rent_id;
		// endif;
		 
		// 	if (!empty($get)) {
		// 		$rp = Rentpayment::create(array(
		// 			'paym_userID' => $userid,
		// 			'paym_rentID' => (!isset($rent['rent_id']))? $nrents : $rent['rent_id'],
		// 			'paym_houseID' => (!isset($input['payment_house']))? 0 : $input['payment_house'],
		// 			'paym_forMonths' => $get['payment']['paym_forMonths'] ,
		// 			'paym_paidAmount' => $get['payment']['amount_paid'],
		// 			'paym_balance' => $get['rent']['rent_balance'],
		// 			'paym_date' => $input['date_paid'],
		// 			'entered_months'=>$input['number_months'],
		// 			'monthsfrom' => $input['date_paid'],
		// 			'monthsto' => $get['payment']['monthsto'],
		// 			'paym_remarks'=>$input['payment_remark']
		// 		));
		// 		if (!empty($rent['rent_firstmonthpaid'])){
		// 				Rent::find($rent['rent_id'])->update(array(
		// 					'rent_nextpaydate' => $get['payment']['monthsto'],
		// 					'rent_lastpaydate' => $input['date_paid'],
		// 					'rent_balance' => $get['rent']['rent_balance']
		// 				));
		// 		}else{
		// 			Rent::find($rent['rent_id'])->update(array(
		// 				'rent_firstmonthpaid' => $input['date_paid'],
		// 				'rent_nextpaydate' => $get['payment']['monthsto'],
		// 				'rent_lastpaydate' => $input['date_paid'],
		// 				'rent_balance' => $get['rent']['rent_balance']
		// 			));
		// 		}
		// 	}else{
		// 		#get is empty
		// 	}
				Flash::overlay('The payment had been made');
				return Redirect::back();
		// pay rent to the house selected
		// check to see if the tenant has a balance or is owing
		// if its a balance add to s{something}
		// if its owing substract from what the tenant pays
		// dont add the deposit ever
		//  create a final number for the payment
		// check and see how many months can he pay for 
		// pay for the number of months that final number can pay for 
		// create a balance figure at all cost. make sure that the tenant does not own
		// if the his payment would not cover his rent 
		// keep the tenant in dept.

	}

	/**
	 * Display the specified resource.
	 * GET /tenanpayments/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /tenanpayments/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /tenanpayments/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /tenanpayments/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}