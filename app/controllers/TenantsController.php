<?php
use Universal\Creation\CreateTenantCommand;
use Universal\Core\CommandBus;
// use services\menus\Topbar;
use Universal\Forms\CreateTenant;
// use Universal\Creation\CreateTenantCommand;
// use Universal\Core\CommandBus;
class TenantsController extends \BaseController {


	use CommandBus;
	protected $CommandBus;
	protected $createtenant;
	protected $layout = 'admin';
	/**
	 * Display a listing of the resource.
	 * GET /tenants
	 *
	 * @return Response
	 */

	function __construct(CreateTenant $createtenant){
		$this->createtenant = $createtenant;
	}
	public function index()
	{
		// $all = House::with('tenants')->get();
		$all = Tenant::with('house','person.contacts')->get();
		$all = ($all)? $all->toArray() : [];
		$this->layout->content = View::make('admin.Tenants.index')->with('tenants',$all);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /tenants/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$data =  array();
		$houses = Compound::with(array('houses'=>function($query){
			$query->where('hous_status','=',0);
		}))->get();
 		$house = ( !empty($houses) )? $houses->toArray() : [];
 		$data['compounds'] = $house;
		$this->layout->content = View::make('admin.Tenants.create')->with('data', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /tenants
	 *
	 * @return Response
	 */
	public function store()
	{
		
		// new method using commander
		// validate
		$this->createtenant->validate(Input::all());
		
		// send the creationg to the createtenant command and then to the bus
		$command = new CreateTenantCommand();
		$this->execute($command);
		// // 

		Flash::message("Successfully added a Tenant");
		return Redirect::back();

	}

	/**
	 * Display the specified resource.
	 * GET /tenants/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$all = Tenant::with('house.compound','person.contacts','person.addresses','rents.payments','person.documents')->whereRaw('tent_id = ? AND deleted = ?',[$id,0])->first();
		//$all = Tenant::with('house.compound','person.contacts','person.addresses','rents.payments')->whereRaw('tent_id = ? AND deleted = ? AND tent_status = ?',[$id,0,0])->first();
		$all = ($all)? $all->toArray() : [];
		// dd($all);
		$this->layout->content = View::make('admin.Tenants.show')->with('tenant',$all);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /tenants/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$all = Tenant::with('house.compound','person.contacts','person.addresses','rents.payments','person.documents')->whereRaw('tent_id = ? AND deleted = ?',[$id,0])->first();
		//$all = Tenant::with('house.compound','person.contacts','person.addresses','rents.payments')->whereRaw('tent_id = ? AND deleted = ? AND tent_status = ?',[$id,0,0])->first();
		$all = ($all)? $all->toArray() : [];
		$this->layout->content = View::make('admin.Tenants.edit')->with('tenant',$all);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /tenants/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		
	$input = Input::all();
	if(isset($input['type'])):
		if($input['type'] == 'person'):
			$person = Person::findOrFail($input['person_id']);
			$person->fill($input);
			$person->save();
			return Redirect::back();
		endif;

		if($input['type'] == 'address'):

			$person = Address::findOrFail( $input['address_id'] );
			$person->fill($input);
			$person->save();
			return Redirect::back();	
		endif;

		if($input['type'] == 'contact'):

			$person = Contact::findOrFail( $input['contact_id'] );
			$person->fill($input);
			$person->save();
			return Redirect::back();

		endif;

		if($input['type'] == 'image' && !empty(Input::file('photo'))):

			$path = (empty($input['foldername']))? base_path().DS.'media/Tenant'.DS.$input['folder'] : base_path().DS.$input['foldername'] ; #notice 006 would fail if not set
			$not =  '';
			$foldername = '';
			$fn = '';
			if(starts_with($input['foldername'], '/')):

				$person = Document::find($input['image_id'])->person()->first();
				$not = 'media'.DS.'Tenant'.DS.$person->pers_fname.'_'.$person->pers_mname.'_'.$person->pers_lname.'_'.$person->id;
				$foldername = DS.$not.DS;
				$fn .= $not;

			else:

				$not = (empty($input['foldername']))? 'media/Tenant'.DS.$input['folder'] : $input['foldername'];
				$foldername = DS.$not.DS;

			endif;

			$thumbnail_dir = $not.DS.'img'.DS.'thumbnail'.DS;

			$fileexten = Input::file('photo')->getClientOriginalExtension();

			$filename = Input::file('photo')->getClientOriginalName();

			$shortened = base_convert(rand(10000,99999), 10, 36);



					if (File::exists($path)) {

					// 	// the folder exist then add a mew  picture and record in databse
					// 	// disable or delete the previous img
							

							


							$rename = 'renamed_'.$shortened.'_photo_'.$filename;

							File::exists($path.DS.'img'.DS.'thumbnail') ?: mkdir($path.DS.'img'.DS.'thumbnail', 0755, true); #notice 007 a hack 

								 Input::file('photo')->move($not.DS.'img'.DS , $rename);

									$img = Image::make($not.DS.'img'.DS.$rename);

									$img->resize(250 , 300);

									$img->save($thumbnail_dir.$rename);	// we move to thumnails

								// store in the database
									// var_dump($thumbnail_dir.$rename);
									// dd($fn);
									// die();
									if(isset($input['image_id'])): 
										
										$document = Document::findOrFail( $input['image_id'] );
										$document->thumnaildir = $thumbnail_dir.$rename;
										$document->fullpath = $input['foldername'].'img'.DS.$rename;
										$document->filename = $filename ;
										if($fn):
											$document->foldername = $not;
										endif;
										$document->extension = $fileexten;
										$document->save();

									else:

										$document = new Document;
										$document->thumnaildir = $thumbnail_dir.$rename;
										$document->entity_type = 'Person';
										$document->type = 'Photo';
										$document->entity_ID = $input['person_id'];
										$document->fullpath = $input['foldername'].'img'.DS.$rename;
										$document->filename = $filename ;
										$document->foldername = $not;
										$document->extension = $fileexten;
										$document->save();

									endif;


								return Redirect::back();	
															
					}else{

							// $full_dir = $foldername.'img'.DS.'full';

							$rename = 'newly_'.$shortened.'_photo_.'.$filename;


							$filetyp = Input::file('photo')->getMimeType();

							$fileexten = Input::file('photo')->getClientOriginalExtension();
							$uploaded = false;


							 File::exists($path) ?: mkdir($path, 0755, true);

							 File::exists($path.DS.'img'.DS) ?: mkdir($path.DS.'img'.DS, 0755, true);

								// var_dump($not.DS.'img'.DS , $rename);
								File::exists($path.DS.'img'.DS.'thumbnail') ?: mkdir($path.DS.'img'.DS.'thumbnail', 0755, true);

								
								// Input::file('photo')->move($not.DS.'img'.DS , $rename);
									// we resize (250 X 300)
								 

								 Input::file('photo')->move($not.DS.'img'.DS , $rename);

									$img = Image::make($not.DS.'img'.DS.$rename);

									$img->resize(250 , 300);

									$img->save($thumbnail_dir.$rename);	// we move to thumnails


									if(isset($input['image_id'])): 

										$document = Document::findOrFail( $input['image_id'] );
										$document->thumnaildir = $thumbnail_dir.$rename;
										$document->fullpath = $input['foldername'].'img'.DS.$rename;
										$document->filename = $filename ;
										$document->foldername = $not;
										$document->extension = $fileexten;
										$document->save();

									else:

										$document = new Document;
										$document->thumnaildir = $thumbnail_dir.$rename;
										$document->fullpath = $input['foldername'].'img'.DS.$rename;
										$document->entity_type = 'Person';
										$document->type = 'Photo';
										$document->entity_ID = $input['person_id'];
										$document->filename = $filename ;
										$document->foldername = $not;
										$document->extension = $fileexten;
										$document->save();

									endif;


					}#img else




		endif;	
	endif;
		if(isset($input['house_add']) && $input['house_add'] == 28):
			$tenant = Tenant::findOrFail($id);
			$tenant->tent_status = 1;
			$tenant->tent_houseID = (isset($input['hous_id']))? $input['hous_id'] : 0;
			$tenant->save();
			$house = House::findOrFail($input['hous_id']);
			$house->hous_status = 1;
			$house->hous_availability = 'not-available';
			$house->hous_tenantID = (!empty($id))? $id: 0;
			$house->save();
			return Redirect::back();
		endif;
		return Redirect::back();	

	}

	public function getVacate($id,$hid){

			$tenant = Tenant::findOrFail($id);
			$tenant->tent_status = 0;
			$tenant->save();
			#you should check to see if the user is owing
			// var_dump($tenant);
			// die();
			$house = House::findOrFail($hid);
			$house->hous_status = 0;
			$house->hous_availability = 'available';
			$house->hous_tenantID =  0;
			$house->save();
		Flash::message("Successfully vacated a Tenant");
		return Redirect::to('\houses');

	}
	/**
	 * Remove the specified resource from storage.
	 * DELETE /tenants/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// check to see if there is a house for the tenant if yes then vacate and and release the house
		Tenant::destroy($id);
		return Redirect::back();
	}
	public function getKill($id)
	{
		Tenant::destroy($id);
		return Redirect::to('\tenants');
	}
	public function getRents($id){
		$rents = [];
		// rent = ['year'=>[months=>[]]]
		$all = Tenant::with('house.compound','person.contacts','person.addresses','rents.payments','person.documents')->whereRaw('tent_id = ? AND deleted = ?',[$id,0])->first();
		//$all = Tenant::with('house.compound','person.contacts','person.addresses','rents.payments')->whereRaw('tent_id = ? AND deleted = ? AND tent_status = ?',[$id,0,0])->first();
		$all = ($all)? $all->toArray() : [];
		$luwas = Tenant::with('luwas')->where('tent_id','=',$id)->first();
		$luwas = ($luwas)? $luwas->toArray() : [];
		if (isset($luwas['luwas']) && !empty($luwas['luwas'])) {
			foreach ($luwas['luwas'] as $key => $value) {
							$all['rent-payment'][$value['year']][$value['month_short']] = $value;
				// $rents['landloard']['compound']['house'][$value['year']][$value['month_short']][] = $value['amount'];
			}
		}

		$this->layout->content = View::make('admin.RentPays.show')->with('tenant',$all);
	}
}