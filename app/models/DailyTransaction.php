<?php
use Laracasts\Commander\Events\EventGenerator;
use Universal\Creation\Events\DailyPaid;
class DailyTransaction extends \Eloquent {
use EventGenerator;
	protected $table = 'daily_transactions';
	protected $primaryKey ='id';
	protected $fillable = ['entityID',
'entityType',
'userID',
'type',
'paid_for',
'customer',
'agentID',
'date_paid',
'locationOrProperty',
'description',
'ref',
'amount',
'house_plot_num'];

public function agent(){
	return $this->belongsTo('Agent','agentID','agen_id');
}
public static function creation($all){
		$input = [];
 		foreach($all as $key => $value)	$input = $value;
		$df = new DailyTransaction;
		$df->fill($input);
		$df->save();
		$df->raise(new DailyPaid($df));
		return $df;
}
}