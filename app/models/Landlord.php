<?php

class Landlord extends \Eloquent {
	protected $primaryKey = 'id';
	protected $fillable = array('ll_personid','ll_fullname','id');
	public function person(){
		return $this->belongsTo('Person','ll_personid','id');
	}
	public function compounds(){
		return $this->hasMany('Compound','comp_landLordID','id');
	}
	public function documents(){
		return $this->hasMany('Document','entity_ID');
	}
	public function kin(){
		return $this->hasOne('Kin','ll_id','id');
	}
	public function luwas(){
		return $this->hasMany('Luwa','ll_id','id');
	}
}