<?php

class Luwa extends \Eloquent {
	protected $primaryKey = 'id';
	protected $fillable = [
			'tenant_id',
			'll_id',
			'house_id',
			'compound_id',
			'month_num',
			'month_short',
			'month',
			'amount',
			'year',
			'deleted'
		];

	public function tenant(){
		return $this->belongsTo('Tenant','tenant_id','tent_id');
	}
	public function landlord(){
		return $this->belongsTo('Landlord','ll_id','id');
	}
}