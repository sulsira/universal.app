<?php
use Laracasts\Commander\Events\EventGenerator;
use Universal\Creation\Events\RentPaid; 
use Universal\Calculations\Rent as R;
class Rentpayment extends \Eloquent {

use EventGenerator;

	protected $table = 'rent_payments';
	protected $fillable = [
				'paym_userID',
				'paym_rentID',
				'paym_houseID',
				'paym_forMonths',
				'paym_paidAmount',
				'paym_balance',
				'paym_currentBal',
				'paym_date',
				'paym_deleted',
				'entered_months',
				'owing',
				'monthsfrom',
				'monthsto',
				'overpay',
				'paym_remarks'
			];

	public function rent(){
		return $this->belongsTo('Rent','paym_rentID','rent_id');
	}
	public static function creation($all){

		$input = [];
 		foreach($all as $key => $value)	$input = $value;

		$rentid = Session::get('rentid');
		$userid = Session::get('user_id');

		$rent = Rent::where('rent_tenantID','=',$input['tent_id'])->first();

		$rent = (!empty($rent))?$rent->toArray(): [];

		$paying = new R($input, $rent );
		$get = $paying->process();
		$nrents = 0;
		if(empty($rent)):
			$nrent = new Rent;
			$nrent->rent_houseID = (!isset($input['payment_house']))? 0 : $input['payment_house'];
			$nrent->rent_tenantID = $input['tent_id'];
			$nrent->save();
			$nrents = $nrent->rent_id;
		endif;
		 
			if (!empty($get)) {
				$rp = Rentpayment::create(array(
					'paym_userID' => $userid,
					'paym_rentID' => (!isset($rent['rent_id']))? $nrents : $rent['rent_id'],
					'paym_houseID' => (!isset($input['payment_house']))? 0 : $input['payment_house'],
					'paym_forMonths' => $get['payment']['paym_forMonths'] ,
					'paym_paidAmount' => $get['payment']['amount_paid'],
					'paym_balance' => $get['rent']['rent_balance'],
					'paym_date' => $input['date_paid'],
					'entered_months'=>$input['number_months'],
					'monthsfrom' => $input['date_paid'],
					'monthsto' => $get['payment']['monthsto'],
					'paym_remarks'=>$input['payment_remark']
				));
				if (!empty($rent['rent_firstmonthpaid'])){
						Rent::find($rent['rent_id'])->update(array(
							'rent_nextpaydate' => $get['payment']['monthsto'],
							'rent_lastpaydate' => $input['date_paid'],
							'rent_balance' => $get['rent']['rent_balance']
						));
				}else{
					Rent::find($rent['rent_id'])->update(array(
						'rent_firstmonthpaid' => $input['date_paid'],
						'rent_nextpaydate' => $get['payment']['monthsto'],
						'rent_lastpaydate' => $input['date_paid'],
						'rent_balance' => $get['rent']['rent_balance']
					));
				}
			}else{
				#get is empty
			}
			// @if(!))
			$rp->raise(new RentPaid($rp));
			return $rp;
	}
}