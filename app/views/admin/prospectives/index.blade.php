@include('templates/top-admin')
@section('content')
	<div class="c-header cc">
		<h3>Prospectives Customers</h3>

	</div>
	<div class="cc">

				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Fullname</th>
							<th>Address</th>
							<th>Contacts</th>
							<th>Interest</th>
							<th>Remarks</th>
							<th>created</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php if (!empty($Partners)): ?>
							<?php foreach ($Partners as $key => $value): ?>
							<tr>
								<td>{{$key+1}}</td>
								<td>{{ucwords($value['name'])}}</td>
								<td><a href="{{route('departments.show',$value['depart_id'])}}">{{ucwords($value['department']['name'])}}</a></td>
								<td><a href="{{route('departments.staffs.show',[$value['depart_id'],$value['staffID']])}}">{{ucwords($value['staff']['Staff_HighestQualification'])}}</a></td>
								<td><?php echo ($value['fee']) ?: 'N/A' ;?></td>
								<td><?php echo ($value['duration']) ?: 'N/A' ;?></td>
								<td><?php echo ($value['qualification']) ?: 'not specified' ;?></td>
								<td><a href="{{route('departments.Partners.show',[$value['depart_id'],$value['id']])}}">View Course</a></td>
							</tr>						
							<?php endforeach ?>
							<?php else: ?>
							<tr>
								<td colspan="8"><h4>No Customers Available!</h4></td>
							</tr>
						<?php endif ?>
					</tbody>
				</table>

		  </div>
		</div>
	</div>
@stop
@include('templates/bottom-admin')