<?php #page specific processing
// dd($data);

 ?>
@include('templates/top-admin')
@section('content')

   <div class="scope">
        <div class="hedacont">
            <div class="navbar">
                <div class="navbar-inner" id="scopebar">
                    <div class="container">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target="navbar-responsive-collapse">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </a>
                        <a class="brand" href="">DATE : 
                        </a>
                        <div class="nav-collapse collapse navbar-responsive-collapse">
                          <ul class="nav">  
                            <li><a href="#basic">All</a> </li>
                            <li><a href="#rent">Income</a></li>
                            <li><a href="#payments">Expenses</a></li>
                            <li class="divider-vertical"></li>
                            <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Options <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                  <li><a href="#">All</a></li>
                              <li><a href="#">Rent only</a></li>
                              <li class="divider"></li>
                              <li><a href="#">Cash mortgage</a></li>
                              <li><a href="#">Mortgage</a></li>
                            </ul>
                          </li>
                           </ul>
                        </div><!-- /.nav-collapse -->
                    </div>
                </div><!-- /navbar-inner -->
            </div> 
         
        </div>  
    </div>  <!-- end of scope -->
    <div class="content-details">
	    <div class="cc clearfix" >
	        <hr>
	        <h3>ALL</h3>
	        <hr id="basic">
	    	<table class="table table-hover table-bordered">
	        <thead>
	            <tr>
	                <th colspan="8">Details</th>
	                <th>Payment</th>
	                <th>Actions</th>
	            </tr>
	            <tr>
	            	<th>Date</th>
	            	<th>Description</th>
	            	<th>Type</th>
	            	<th>For:</th>
	            	<th>Locate</th>
	            	<th>Agent</th>
	            	<th>Customer</th>
	            	<th>Ref#</th>
	            	<th>Amount</th>
	            	<th>Options</th>
	            </tr>
	        </thead>
	        <tbody>
	        <tr>
	        	<td>jan-28</td>
	        	<td>soething else</td>
	        	<td>Income</td>
	        	<td>Rent</td>
	        	<td>serrekunda</td>
	        	<td>Fatou cessay</td>
	        	<td>Ebrima manneh</td>
	        	<td>r001</td>
	        	<td>2000</td>
	        	<td><a href="">view</a></td>
	        </tr>
	        </tbody>
	        <tfoot>
	        	<tr>
	        		<th>Total</th>
	        		<th>-</th>
	        		<th>-</th>
	        		<th>-</th>
	        		<th>-</th>
	        		<th>-</th>
	        		<th>6</th>
	        		<th>-</th>
	        		<th>2000</th>
	        		<th>-</th>
	        	</tr>
	        </tfoot>
	   		</table>
	    </div> <!-- a .cc -->
  <div class="cc clearfix" id="rent">
    <h3>Income</h3>
    <hr>
  </div>
   <div class="cc clearfix" id="payments">
        <h3>Expenses</h3>
    <hr>

   </div>



</div>


@stop
@include('templates/bottom-admin')